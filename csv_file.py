# CSV(Comma separated variables) files
import csv
# Opening the file
example = open('/Users/sabitamanandhar/Desktop/pythonProject/working on spreadsheet/example.csv', encoding=('utf-8'))
# converting it into csv data using csv.reader
csv_example = csv.reader(example)
# re-formatting the data into python object list of lists
final_example = list(csv_example)

#  print(final_example) shows all the data but in raw form
# below shows the actual column names
print(final_example[0])
# formatted nicely 5 rows
for line in final_example[:5]:
    print(line)

# Extracting a row. It shows exactly the 10th column and works nicely because index 0 is the column names/topics.
print(final_example[10])

# Extracting the single value/ certain persons email in the list
print(final_example[10][3])

# Extracting certain whole column
all_emails = []
for list in final_example[1:]:
    all_emails.append(list[3])
print(all_emails)

# Combining two columns e.g. if we need the names and surnames combine
# displaying the full-names.
full_names = []
for list in final_example[1:]:
    full_names.append(list[1]+' '+list[2])
print(full_names)